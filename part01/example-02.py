# example-02.py
import sys
from PySide2.QtWidgets import QApplication, QLabel, QLineEdit
from PySide2.QtWidgets import QDialog, QPushButton, QVBoxLayout, QHBoxLayout

class HelloWorld(QDialog):

    def __init__(self, parent=None):
        super(HelloWorld, self).__init__(parent)



        vbox_layout = QVBoxLayout()

        salutation_layout = QHBoxLayout()
        salutation_label = QLabel("Salutation:")
        self.salutation = QLineEdit("")
        salutation_layout.addWidget(salutation_label)
        salutation_layout.addWidget(self.salutation)

        name_layout = QHBoxLayout()
        name_label = QLabel("Name:")
        self.name = QLineEdit("")
        name_layout.addWidget(name_label)
        name_layout.addWidget(self.name)

        self.button = QPushButton("Print")

        vbox_layout.addLayout(salutation_layout, stretch=1)
        vbox_layout.addLayout(name_layout, stretch=1)
        vbox_layout.addWidget(self.button)

        self.setLayout(vbox_layout)

        self.button.clicked.connect(self.hello_world)

    def hello_world(self):
        message = self.salutation.text() + ", " + self.name.text() + "!"
        print(message)


if __name__ == '__main__':
    app = QApplication([])
    helloworld = HelloWorld()
    helloworld.show()
    sys.exit(app.exec_())
