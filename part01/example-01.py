# example-01.py: Hello World, Version 1
import sys
from PySide2.QtWidgets import QApplication, QLabel

if __name__ == '__main__':
    app = QApplication([])
    label = QLabel("PySide2 aka Qt for Python!")
    label.show()
    sys.exit(app.exec_())
