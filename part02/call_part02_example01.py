import sys
from PySide2.QtWidgets import QApplication, QDialog
from part02_example01 import Ui_Dialog


class MyForm(QDialog):
    def __init__(self):
       super().__init__()
       self.ui = Ui_Dialog()
       self.ui.setupUi(self)
       self.ui.buttonPrint.clicked.connect(self.hello_world)
       self.show()

    def hello_world(self):
        self.ui.labelMessage.setText( 
            self.ui.lineEditSalutation.text() + ", " + 
            self.ui.lineEditName.text() + "!")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
