# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'part02-example01.ui',
# licensing of 'part02-example01.ui' applies.
#
# Created: Sun Mar 17 17:33:54 2019
#      by: pyside2-uic  running on PySide2 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(508, 300)
        self.buttonPrint = QtWidgets.QPushButton(Dialog)
        self.buttonPrint.setGeometry(QtCore.QRect(150, 220, 171, 33))
        self.buttonPrint.setObjectName("buttonPrint")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(20, 30, 141, 24))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(20, 90, 99, 24))
        self.label_2.setObjectName("label_2")
        self.labelMessage = QtWidgets.QLabel(Dialog)
        self.labelMessage.setGeometry(QtCore.QRect(20, 150, 461, 24))
        self.labelMessage.setText("")
        self.labelMessage.setObjectName("labelMessage")
        self.lineEditSalutation = QtWidgets.QLineEdit(Dialog)
        self.lineEditSalutation.setGeometry(QtCore.QRect(140, 30, 241, 32))
        self.lineEditSalutation.setObjectName("lineEditSalutation")
        self.lineEditName = QtWidgets.QLineEdit(Dialog)
        self.lineEditName.setGeometry(QtCore.QRect(140, 90, 241, 32))
        self.lineEditName.setObjectName("lineEditName")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "Dialog", None, -1))
        self.buttonPrint.setText(QtWidgets.QApplication.translate("Dialog", "Print", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("Dialog", "Salutation", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("Dialog", "Name", None, -1))

